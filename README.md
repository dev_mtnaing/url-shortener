# README

* Ruby version
2.3.3

This was written as part of interview test. Just pretty basic feature of shortening url.

# Demo #
* [Demo](https://mtnaing-urlshortener.herokuapp.com)

# API Doc #
* [API Doc](https://documenter.getpostman.com/view/58898/url-shortener/6fctfjs)