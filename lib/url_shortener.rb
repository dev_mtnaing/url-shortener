require 'url_shortener/config'
require 'url_shortener/core'

module UrlShortener

  # def self.included(model_class)
  #   model_class.extend self
  # end

  def self.config(&block)
    if block_given?
      block.call UrlShortener::Config 
    else
      UrlShortener::Config
    end
  end

end