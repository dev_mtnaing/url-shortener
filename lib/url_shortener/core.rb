require 'url_shortener/config'

module UrlShortener
  autoload :UrlHasher, 'url_shortener/url_hasher'

  class Core

    attr_accessor :url_shortener_config, :url_hasher

    def initialize(url_hasher: UrlHasher.new)
      @url_shortener_config = UrlShortener::Config
      @url_hasher = url_hasher 
    end

    def build_shortened_object(long_url)
      hash_key = generate_hash_key 
      long_url = parse_long_url(long_url)
      return {
        long_url: long_url,
        hash_key: hash_key,
        short_url: "#{@url_shortener_config.base_url}/#{hash_key}",
        domain: get_domain(long_url),
      }
    end

    def generate_hash_key
      @url_hasher.generate_hash_key
    end

    private
      def get_domain(url)
        URI.parse(url).host
      end

      def parse_long_url(long_url)
        URI.parse(long_url).scheme ? long_url : "http://#{long_url}"
      end

  end
end