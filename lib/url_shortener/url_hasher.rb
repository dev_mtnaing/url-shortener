module UrlShortener
  class UrlHasher
    def generate_hash_key
      SecureRandom.urlsafe_base64 6
    end
  end
end