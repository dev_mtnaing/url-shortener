module UrlShortener
  module Config
    class << self 
      attr_accessor :base_url

      def setup
        @base_url = 'http://localhost:9000'
      end
    end

    setup
  end
end