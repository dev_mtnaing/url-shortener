class CreateLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :links do |t|
      t.string :long_url
      t.string :short_url
      t.string :hash_key
      t.integer :clicks, default: 0
      t.string :title
      t.string :domain
      t.references :user, index: true, foreign_key: true

      t.timestamps
    end
    add_index :links, :hash_key, unique: true
    add_index :links, :domain
  end
end
