class CreateLinkClicks < ActiveRecord::Migration[5.0]
  def change
    create_table :link_clicks do |t|
      t.references :link, foreign_key: true
      t.references :user, foreign_key: true
      t.references :visit, foreign_key: true

      t.timestamps
    end
  end
end
