class RemoveLinkIdsFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :link_ids
  end
end