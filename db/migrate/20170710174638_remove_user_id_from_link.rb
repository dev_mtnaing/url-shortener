class RemoveUserIdFromLink < ActiveRecord::Migration[5.0]
  def change
    remove_index :links, :user_id
    remove_column :links, :user_id
  end
end
