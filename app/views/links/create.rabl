node(:status) {'success'}
child @link => :data do
  attribute :id, :long_url, :short_url, :title, :hash_key, :domain, :clicks
end
