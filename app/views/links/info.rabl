node(:status) {'success'}
child @link => :data do
  attribute :id, :long_url, :short_url, :title, :hash_key, :domain, :clicks

  @links = Link.where(long_url: @link.long_url)
  @link_clicks = @link.link_clicks

  node(:total_clicks) do
    @links.sum(:clicks)
  end

  node(:total_links) do
    @links.count
  end

  child @link_clicks, object_root: false do |lc|
    object @link_clicks
    attribute :id, :created_at

    child :visit do
      attribute :id, :ip, :country, :browser, :referrer, :landing_page, :region, :city, :postal_code, :latitude, :longitude
    end
  end
end