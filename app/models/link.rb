class Link < ApplicationRecord
  has_many :link_clicks, dependent: :destroy
  # in case if the base url change and the pre saved short_url in database doesn't work anymore
  def short_url
    "#{UrlShortener::Config.base_url}/#{self.hash_key}"
  end
end