class LinkClick < ApplicationRecord
  belongs_to :link
  belongs_to :user, optional: true
  belongs_to :visit, optional: true

  visitable
end