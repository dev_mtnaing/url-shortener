class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_many :user_links, dependent: :destroy
  has_many :links, through: :user_links

  def recent_links
    self.links.order('user_links.updated_at DESC')
  end

end