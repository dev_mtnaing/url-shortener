// handle shorten button click
$('#shorten-btn').on('click', function() {
  showLoader();
  $.ajax({
    type: "POST",
    url: '/v1/links',
    headers: {
      'access-token': access_token,
      'client': client,
      'uid': uid
    },
    data: { long_url: $('#shorten-input').val() },
    success: function(link) {
      hideLoader();
      if ( !isLinkInObject(JSON.parse(links), link.data) ) {
        $('#shorten-list').prepend(`
          <li class="list-group-item">
            ${link.data.title || link.data.long_url}
            <br />
            <a href='${link.data.long_url}'>${link.data.long_url}</a>
            <br />
            <div class="shorten-link">
              <span class="glyphicon glyphicon-signal pull-right" aria-hidden="true"></span>
              <a href='${link.data.short_url}+' class="pull-right">${link.data.clicks}</a>
            </div>
            <span class="shorten-link">${link.data.short_url}</span>
          </li>
        `);
      }
    },
    error: function(err) {
      hideLoader();
      $('.flash-message-div').append(`
        <div class='alert alert-danger alert-dismissible'>
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
          ${JSON.parse(err.responseText).errors.full_messages[0]}
        </div>
      `);
    },
  });
});

// handle Enter key press on shorten input field
$('#shorten-input').keypress(function (e) {
 var key = e.which;
 if (key == 13) {
    $('#shorten-btn').click();
    return false;
  }
});

function showLoader() {
  $('#loader').css('display', 'block');
}

function hideLoader() {
  $('#loader').css('display', 'none');
}

function isLinkInObject(obj, link) {
  console.log(obj, link);
  for (var key in obj) {
    if (obj[key].id === link.id) {
      return true;
    }
  }
  return false;
}