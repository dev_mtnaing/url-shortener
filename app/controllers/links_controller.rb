class LinksController < ApplicationController
  before_action :track_visit, only: [:go]

  def index
    @user = current_or_guest_user
    @links =  @user.recent_links

    # setting up tokens since we are calling our own api route
    if @user.tokens.any?
      new_auth_header = @user.create_new_auth_token(@user.tokens.first[0])
    else
      new_auth_header = @user.create_new_auth_token()
    end
    @access_token = new_auth_header["access-token"]
    @client = new_auth_header["client"]
    @uid = new_auth_header["uid"]
  end

  def go
    params.require(:hash_key)

    @link = Link.find_by(hash_key: params[:hash_key].chomp('+'))
    if @link.nil?
      render "errors/not_found"
    else
      @links = Link.where(long_url: @link.long_url)
      if params[:hash_key].last == '+' # if params end with '+' then show short_url details
        render 'links/show'
      else # if not then redirect to long_url
        redirect_to @link.long_url
        # track clicks
        @link.clicks += 1
        @link.save
        LinkClick.create!(link_id: @link.id, visit_id: ahoy.visit.id)
      end
    end
  end

  private
    def track_visit
      # Since no ahoy.visit.id if the visitor is new and go straight to redirect_link
      if Visit.find_by(visit_token: ahoy.visit_token).nil?
        ahoy.track_visit
      end
    end

end