module Api
  module V1
    class UsersController < ApiController
      before_action :authenticate_user!

      def info
      end

      def link_history
      end
    end
  end
end