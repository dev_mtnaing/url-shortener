module Api
  module V1
    class LinksController < ApiController

      def info
        params.require(:short_url)

        hash_key = params[:short_url].split('/').last
        @link = Link.find_by(hash_key: hash_key)

        if @link
          render "links/info"
        else
          render json: { status_code: 404, errors: [ 'Links not found' ] }, status: 404
        end
      end

      def create
        params.require(:long_url)

        result = CreateShortenUrl.call long_url: params[:long_url], current_user: @user

        if result.success?
          @link = result.link
          render 'links/create'
        else
          raise Exception.new(result.message)
        end
      end
    end
  end
end