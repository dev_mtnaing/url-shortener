module Api
    class ApiController < ActionController::Base
      # include DeviseTokenAuth::Concerns::SetUserByToken
      before_action :set_user_by_token

      rescue_from Exception do |exception|
        render json: { status: 'error', errors: { full_messages: [exception] } }, status: 400
      end

      # just simple setting user by passed access_token, uid, and client
      # Because Devisetokenauth::setuserbytoken call devise sign_in method (which i don't want)
      private
        def set_user_by_token
          uid       = request.headers[:uid] || params[:uid]
          token     = request.headers['access-token'] || params['access-token']
          client = request.headers[:client] || params[:client]

          user = User.find_by(uid: uid)

          if user && user.valid_token?(token, client)
            @user = user
          end
        end
    end
end