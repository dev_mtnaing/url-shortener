Dir["#{Rails.root}/app/interactors/CreateShortenUrlInteractors/*.rb"].each {|file| require file }

class CreateShortenUrl
  include Interactor::Organizer
  organize BuildLinkObject, CheckUrlStatus, RetrieveDetailsFromWebpage, CreateLinkRecord, MapUserAndLink
end
