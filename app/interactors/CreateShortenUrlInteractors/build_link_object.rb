class BuildLinkObject
  include Interactor

  before do
    # params validation
    if !context.long_url || context.long_url.empty?
      context.fail!(message: 'long_url cannot be empty')
    end
  end

  def call
    shortener = UrlShortener::Core.new
    context.link_object = shortener.build_shortened_object(context.long_url.strip)
  end
end