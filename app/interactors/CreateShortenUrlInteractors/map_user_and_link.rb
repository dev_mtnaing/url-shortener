class MapUserAndLink
  include Interactor

  before do
    # params validation
    if !context.link
      context.fail!(message: 'Link is required')
    end
  end

  def call
    # add the link to user if current_user is given
    unless context.current_user && context.current_user.nil?
      user_link = UserLink.find_by(link_id: context.link.id, user_id: context.current_user.id)
      if user_link
        # if any user_link already there then update the updated_at so that it comes first in the listing
        user_link.updated_at = Time.now
        user_link.save
      else
        context.current_user.user_links.create(link_id: context.link.id)
      end
    end

    return context.link
  end
end