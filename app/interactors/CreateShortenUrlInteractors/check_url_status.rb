class CheckUrlStatus
  include Interactor

  before do
    # params validation
    if !context.link_object
      context.fail!(message: 'provide link object to create record')
    end
  end

  def call
    agent = Mechanize.new
    begin
      context.page = agent.get(context.link_object[:long_url])
      rescue
        context.fail!(message: 'Web page does not exist. please check again.')
    end
  end
end
