class CreateLinkRecord
  include Interactor

  before do
    # params validation
    if !context.link_object
      context.fail!(message: 'provide link object to create record')
    end

    # add page details to link_object
    if context.page_details
      context.link_object[:title] = context.page_details[:title]
    end

    if context.current_user && !context.current_user.encrypted_password.empty?
      context.link_object[:user_id] = context.current_user.id
    end
  end

  def call
    # create new record if user is not guest
    if context.link_object[:user_id]
      puts 'user id here'
      link = Link.find_by(user_id: context.current_user.id, long_url: context.link_object[:long_url])
      context.link = create_or_update_link(link, context.link_object)
    else
      puts 'user id not here'
      link = Link.find_by(long_url: context.link_object[:long_url])
      context.link = create_or_update_link(link, context.link_object)
    end
  end

  private
    def create_or_update_link(link, link_object)
      if link.nil?
        count = 0
        begin
          link = Link.create!(link_object)
        rescue ActiveRecord::RecordNotUnique, ActiveRecord::StatementInvalid => err
          BuildLinkObject.call long_url: link_object[:long_url]
          if (count +=1) < 5
            retry
          else
            context.fail!(message: "Please try again later due to too many failures on creating short url with unique hash key")
          end
        end
      else
        link.update(title: link_object[:title])
      end

      return link
    end

end