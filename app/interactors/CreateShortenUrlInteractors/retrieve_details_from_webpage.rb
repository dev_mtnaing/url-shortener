class RetrieveDetailsFromWebpage
  include Interactor

  before do
    # params validation
    if !context.page
      context.fail!(message: 'Page need to be passed to retrieve details')
    end
  end

  def call
    context.page_details = {
      title: context.page.title
    }
  end
end