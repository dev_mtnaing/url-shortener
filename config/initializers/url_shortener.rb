UrlShortener.config do |config|
  config.base_url =  Rails.env.development? ? 'http://localhost:3000' : 'https://mtnaing-urlshortener.herokuapp.com'
end