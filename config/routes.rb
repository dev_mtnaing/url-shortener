Rails.application.routes.draw do
  devise_for :users

  # Link Routes
  get '/', to: 'links#index'
  get '/:hash_key', to: 'links#go'

  # API routes
  namespace :api, path: nil, defaults: {format: 'json'} do
    namespace :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      mount_devise_token_auth_for 'User', at: 'auth'

      # Link routes
      resources :links, only: [:create] do
        collection do
          get 'info', to: 'links#info'
          get 'lookup', to: 'links#lookup'
        end
      end

      # User routs
      get "users/info", to: "users#info"
      get "users/link_history", to: "users#link_history"

    end
  end

end